from setuptools import setup

setup(
    name= 'fitderiv',
    py_modules= ['fitderiv', 'fitderivgui'],
    author= 'Peter Swain',
    author_email= 'peter.swain@ed.ac.uk',
    url= '',
    license='',
    description= 'A module that uses Gaussian processes to estimate first and second derivatives',
    python_requires= '>=3.6',
    include_package_data= True,
    install_requires= [
        'numpy',
        'matplotlib',
        'pandas',
        'gaussianprocess@git+https://git.ecdf.ed.ac.uk/pswain/gaussianprocess@master',
        'genutils@git+https://git.ecdf.ed.ac.uk/pswain/genutils@master'
        ]
)
